#include "bt.h"


int finalPoda=0;
int final=0;

int resolverPuro(vector<int> s){
	final  = s.size();
	if(s.size() <= 2) return 0;
	else{
		subsecRec(s, 0, -1, -1, 0);
		return final;
	}
}

int resolverPoda(vector<int> s){
	finalPoda  = s.size();
	if(s.size() <= 2) return 0;
	else{
		subsecRecPoda(s, 0, -1, -1, 0);
		return finalPoda;
	}
}

void subsecRec(vector<int> s, int etapa, int ultRojo, int ultAzul, int cantCeros){
	
	int n = s.size();
	for(int i= 2; i>-1; i--){

		if(agregueIvalido(s, etapa, ultRojo, ultAzul, i)){
			if( etapa == n-1){
				if( i == 0 ) checkSolucion(cantCeros+1);
				if( i == 1 ) checkSolucion(cantCeros);
				if( i == 2 ) checkSolucion(cantCeros);
			}
			else{
				if( i == 0 ) subsecRec(s, etapa +1, ultRojo, ultAzul, cantCeros +1);
				if( i == 1 ) subsecRec(s, etapa +1, etapa, ultAzul, cantCeros);
				if( i == 2 ) subsecRec(s, etapa +1, ultRojo, etapa, cantCeros);
			}
		}
		
	}
	
}


void subsecRecPoda(vector<int> s, int etapa, int ultRojo, int ultAzul, int cantCeros){
	int n = s.size();
	for(int i= 2; i>-1; i--){

		if(agregueIvalido(s, etapa, ultRojo, ultAzul, i)){
			if( etapa == n-1){
				if( i == 0 ) checkSolucionPoda(cantCeros+1);
				if( i == 1 ) checkSolucionPoda(cantCeros);
				if( i == 2 ) checkSolucionPoda(cantCeros);
			}
			else{
				if(cantCeros < finalPoda){
					if( i == 0 ) subsecRecPoda(s, etapa +1, ultRojo, ultAzul, cantCeros +1);
					if( i == 1 ) subsecRecPoda(s, etapa +1, etapa, ultAzul, cantCeros);
					if( i == 2 ) subsecRecPoda(s, etapa +1, ultRojo, etapa, cantCeros);
				}
			}
		}
		
	}
}


void checkSolucion(int cantCeros){ // O(1)  
	if(cantCeros < final) final = cantCeros;
}


void checkSolucionPoda(int cantCeros){ // O(1)
	if(cantCeros < finalPoda) finalPoda = cantCeros;
}


bool agregueIvalido(vector<int> s, int etapa, int ultRojo, int ultAzul, int i){ //O(1)

	if(i == 0) return true;
	else if(i == 1){
		if( ultRojo == -1 ) return true;
		else if(s[ultRojo] < s[etapa]) return true;
		else false;
	}else{
		if( ultAzul == -1 ) return true;
		else if(s[ultAzul] > s[etapa]) return true;
		else false;
	}
} 