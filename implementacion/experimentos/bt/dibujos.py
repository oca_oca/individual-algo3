import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

tiemposPoda = "./tiemposPodaRandom.dat"
tiemposPuro = "./tiemposPuroRandom.dat"

def promedio(e):
    g = e.split(';')
    res = 0
    for i in g[1:]:
        res = res + int(i)
    res = res / (len(g)-1)
    return res

def armar_a(f):
    a = f.readlines()
    b = []
    for i in a[1:]:
        b.append(promedio(i))
        
    return b

if __name__ == '__main__':
    f = open(tiemposPoda, 'r')
    datos_poda = armar_a(f)
    poda = pd.Series(datos_poda)
    poda.plot(color='green', label='con poda')
    
    plt.xlabel('Tam de entrada (n)')
    plt.ylabel('Tiempo')
    plt.title('Promedio de 100 repeticiones random por cada n')
    plt.yscale('log')
    f.close
   
    f = open(tiemposPuro, 'r')
    datos_puro = armar_a(f)
    puro = pd.Series(datos_puro)
    puro.plot(color='magenta',label='sin poda')
    
    plt.legend(loc='upper left')
    plt.savefig("./poda_vs_puro.jpg")
    plt.show()
    ###################################
