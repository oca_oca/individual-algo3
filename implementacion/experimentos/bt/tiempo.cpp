
#include "bt.h"
#include <chrono>
#include <stdlib.h>
#include <vector>
#include <utility>
#include <iostream>


#define ya chrono::high_resolution_clock::now

using namespace std;

// ./tiempo repeticiones P(puro)/_(poda) n1 n2 n3 ...
// imprime los resultados de tiempo por stdout en formato csv ;

int main(int argc, char const *argv[])
{
	// interpretamos los parametros
	int repes = atoi(argv[1]);
	bool puro = argv[2][0] == 'P';
	vector<int> ns(argc-3);
	for (int i = 0; i < ns.size(); i++) {
		ns[i] = atoi(argv[i+3]);
	}
	
	// escribimos el header del csv
	cout << "n;";
	for (int i = 1; i <= repes; ++i)
		cout << "r" << i << ";";
	cout <<endl;
	// corremos los experimentos y vamos escupiendo los datos
	for (auto n : ns) {
		int solucion;
		cout << n << ';';
		for (int t = 0; t < repes; t++) {
			cerr << "Empezando medicion nro "<< t+1 << " para n = " << n << endl;
			vector<int> s;
			//int e; // va a ser mi elemento random de la sucesion
			for(int r = t*t; r <t*t + n; r++){
				//e = rand();
				s.push_back(r);
			}
			auto start = ya();
			if (puro) {
				solucion = resolverPuro(s);
			} else {
				solucion = resolverPoda(s);
			}
			auto end = ya();
			if(t < repes-1){
				cout << chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << ";";
			}else{
				cout << chrono::duration_cast<std::chrono::nanoseconds>(end-start).count();
			} 
		}
		cout <<endl;
		
	}
}


