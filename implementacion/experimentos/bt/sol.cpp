#include "bt.h"

#include <stdlib.h>
#include <vector>
#include <iostream>

using namespace std;

// ./sol P(puro)/_(poda) s_0 s_1 s_2 ...
int main(int argc, char const *argv[]) {

	bool puro = argv[1][0] == 'P';
	vector<int> s(argc-2); // long de la secuencia es el total de parametros menos el ejecutable y P/X

	for(int i = 0; i < s.size(); i++){
		s[i] = atoi(argv[i+2]);
	}
	int solucion(puro ? resolverPuro(s) : resolverPoda(s)); 
	cout << solucion << endl;
	return solucion;
}
