#ifndef BT_H_INCLUDED
#define BT_H_INCLUDED
#include <vector>
#include <iostream>

using namespace std;

int resolverPuro(vector<int> s);

int resolverPoda(vector<int> s);

void subsecRec(vector<int>, int etapa, int ultRojo, int ultAzul, int cantCeros);

void subsecRecPoda(vector<int>, int etapa, int ultRojo, int ultAzul, int cantCeros);

void checkSolucion(int cantCeros);

void checkSolucionPoda(int cantCeros);

bool agregueIvalido(vector<int> s, int etapa, int ultRojo, int ultAzul, int i);

#endif // BT_H_INCLUDED