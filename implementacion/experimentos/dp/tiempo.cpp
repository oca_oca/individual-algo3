
#include "pd.h"
#include <chrono>
#include <stdlib.h>
#include <vector>
#include <utility>
#include <iostream>


#define ya chrono::high_resolution_clock::now

using namespace std;

// ./tiempo repeticiones I/R n1 n2 n3 ...
// imprime los resultados de tiempo por stdout en formato csv ;

int main(int argc, char const *argv[])
{
	// interpretamos los parametros
	int repes = atoi(argv[1]);
	bool iterativo = argv[2][0] == 'I';
	vector<int> ns(argc-3);
	for (int i = 0; i < ns.size(); i++) {
		ns[i] = atoi(argv[i+3]);
	}
	
	// escribimos el header del csv
	cout << "n;";
	for (int i = 1; i <= repes; ++i)
		cout << "r" << i << ";";
	cout <<endl;
	// corremos los experimentos y vamos escupiendo los datos
	for (auto n : ns) {
		int solucion;
		cout << n << ';';
		for (int t = 0; t < repes; t++) {
			cerr << "Empezando medicion nro "<< t+1 << " para n = " << n << endl;
			vector<int> s;
			int e; // va a ser mi elemento random de la sucesion
			for(int r = 0; r < n; r++){
				e = rand();
				s.push_back(e);
			}
			auto start = ya();
			if (iterativo) {
				solucion = pdIterativo(s);
			} else {
				solucion = pdRecursivo(s);
			}
			auto end = ya();
			if(t < repes-1){
				cout << chrono::duration_cast<std::chrono::nanoseconds>(end-start).count() << ";";
			}else{
				cout << chrono::duration_cast<std::chrono::nanoseconds>(end-start).count();
			} 
		}
		cout <<endl;
	}
}


