#include "pd.h"

#include <stdlib.h>
#include <vector>
#include <iostream>

using namespace std;

// ./sol I/R s_0 s_1 s_2 ...
int main(int argc, char const *argv[]) {
	bool iterativo = argv[1][0] == 'I';
	vector<int> s(argc-2);
	for(int i = 0; i < s.size(); i++){
		s[i] = atoi(argv[i+2]);
	}
	int solucion(iterativo ? pdIterativo(s) : pdRecursivo(s)); 
	cout << solucion<<endl;
	return solucion;
}
