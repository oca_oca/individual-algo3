import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

tiemposIterativo = "./tiemposIterativo.dat"
tiemposRecursivo = "./tiemposRecursivo.dat"

def promedio(e):
    g = e.split(';')
    res = 0
    for i in g[1:]:
        res = res + int(i)
    res = res / (len(g)-1)
    return res

def armar_a(f):
    a = f.readlines()
    b = []
    for i in a[1:]:
        b.append(promedio(i))
        
    return b

if __name__ == '__main__':
    f = open(tiemposIterativo, 'r')
    datos_it = armar_a(f)
    it = pd.Series(datos_it)
    it.plot()
    plt.xlabel('Tam de entrada (n)')
    plt.ylabel('Tiempo')
    plt.title('Promedio de 100 repeticiones random por cada n')
    f.close
    # f = open(tiemposRecursivo, 'r')
    # datos_rec = armar_a(f)
    # rec = pd.Series(datos_rec)
    # rec.plot()
    plt.savefig("./Iterativo.jpg")
    plt.show()
    # ###################################
