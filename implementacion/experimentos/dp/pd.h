#ifndef PD_H_INCLUDED
#define PD_H_INCLUDED
#include <vector>
#include <iostream>

using namespace std;

int pdRecursivo(vector<int> s);

int pdIterativo(vector<int> s);

int maxLong( vector<int> s);

int calcular(vector<int> s,vector<vector<int> > M, int i, int j);

#endif // PD_H_INCLUDED