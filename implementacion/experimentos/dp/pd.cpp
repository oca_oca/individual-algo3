#include "pd.h"

int pdRecursivo(vector<int> s){
	return 0;
}

int pdIterativo(vector<int> s){
	if(s.size() <= 2) return 0;
	else{
		return s.size()-maxLong(s);
	}
}

int maxLong( vector<int> s){
	int n = s.size();
	vector<vector<int> > M(n+1, vector<int>(n+1));
	//inicializo mi matriz base (caso base)
	M[0][0] = 0; //Si no hay  numeros para pintar, el max que puedo pintar es 0
	
	for(int i=0; i<= n; i++){ //cicla n veces O(n²), osea O(n³)
		
		for(int j=0; j< i; j++){ // i está acotado por n, asi que el ciclo es O(n²)
			M[i][j] = calcular(s, M, i, j) ; // O(n)
			M[j][i] = calcular(s, M, j, i) ; // O(n)
		}
		
		if(i > 0){
			// Calculo el maximo de la submatriz hasta i 
			// y lo pongo en la diagonal M[i][i]
			int maxSubMatriz = M[i-1][i-1]; 	// Por defecto le pongo el max de la submatriz anterior
			for(int m = 0; m < i; m++){ 		// i está acotado por n, asi que el ciclo es O(n)
				if( M[i][m] > maxSubMatriz ) maxSubMatriz = M[i][m]; //O(1)
				if( M[m][i] > maxSubMatriz ) maxSubMatriz = M[m][i]; //O(1)
			}
			M[i][i] = maxSubMatriz; //O(1)
		}
	}
	return M[n][n] ;
}

int calcular(vector<int> s, vector<vector<int> > M, int i, int j){
	bool mejoroAlguno = false;
	int maxLongitud = 0;
	int max = 0;
	if(i<j){
		mejoroAlguno = false;
		for(int k=j-1; k>0; k--){ // j-1 está acotado por n, asi que el ciclo es O(n)
			if( k!= i && s[j-1] < s[k-1]){ // O(1) es una comparacion de int
				mejoroAlguno = true;
				if( M[i][k] > maxLongitud){ //O(1) es una comparacion de int
					maxLongitud = M[i][k];
				}
			}
		}
		if( mejoroAlguno) max= maxLongitud +1;
		else max= (M[i][0] +1);
	}
	if(i>j){
		mejoroAlguno = false;
		for(int k=i-1; k>0; k--){
			if( k!= j && s[i-1] > s[k-1]){
				mejoroAlguno = true;
				if( M[k][j] > maxLongitud){
					maxLongitud = M[k][j];
				}
			}
		}
		if( mejoroAlguno) max= maxLongitud +1;
		else max= (M[0][j] +1);

	}
	
	return max;
}
