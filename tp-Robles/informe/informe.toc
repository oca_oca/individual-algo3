\select@language {spanish}
\contentsline {section}{\numberline {1}Introducci\IeC {\'o}n}{3}{section.1}
\contentsline {section}{\numberline {2}Orden M\'aximo}{3}{section.2}
\contentsline {section}{\numberline {3}Riffle Shuffle}{4}{section.3}
\contentsline {section}{\numberline {4}Cota para el Orden M\'aximo}{4}{section.4}
\contentsline {section}{\numberline {5}Estimaci\IeC {\'o}n de valor esperado y desv\IeC {\'\i }o est\IeC {\'a}ndar del orden m\IeC {\'a}ximo}{5}{section.5}
\contentsline {section}{\numberline {6}Estimaci\IeC {\'o}n de valor esperado y desv\IeC {\'\i }o standar usando Riffle Schuffle n veces}{5}{section.6}
\contentsline {section}{\numberline {7}\IeC {\textquestiondown }Cu\IeC {\'a}ntas veces mezclar para tener un mazo aleatorio?}{6}{section.7}
